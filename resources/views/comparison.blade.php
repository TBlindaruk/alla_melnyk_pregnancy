@extends('layouts.app')

@section('content')
    <div class="container comparison">
        <div class="row flex-center">
            <h1>{{$question->question}}</h1>
        </div>
        <form action="/comparison/vote" method="POST" onsubmit="if(!onSubmit(this)){return false}">
            <div class="content row">
                <input type="hidden" name="question" value="{{$question->id}}">
                <?php $candidatesIds = [] ?>
                <?php $candidatesNames = [] ?>
                @foreach($candidates as $index => $candidate)
                    <?php $candidatesIds[$index] = $candidate->id?>
                    <?php $candidatesNames[$index] = $candidate->name?>
                    <div class="col-sm-6">
                        <img src="{{$candidate->img_url}}">
                        <a
                                target="_blank"
                                href="{{$candidate->link}}">
                            <h2>
                                <span>{{$candidate->name}}</span>
                                <span class="circle">({{$questionCandidates[$index]->rating}})</span>
                            </h2>
                        </a>
                    </div>
                @endforeach
            </div>
            <div>
                {{ csrf_field() }}
                <div class="g-recaptcha"
                     data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"
                     data-callback="reCaptcha"
                >
                </div>
                <button
                        name="submit"
                        value="{{sprintf('%s,%s',$candidatesIds[0],$candidatesIds[1])}}"
                        class="btn first vote">
                    <span><?=$candidatesNames[0] ?></span>
                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                </button>
                <button
                        name="submit"
                        value="{{sprintf('%s,%s',$candidatesIds[1],$candidatesIds[0])}}"
                        class="btn second vote">
                    <span><?=$candidatesNames[1] ?></span>
                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                </button>
            </div>
        </form>
    </div>

    <div class="fb-share-button">
        <iframe src="https://www.facebook.com/plugins/share_button.php?href=https://wobo.herokuapp.com&layout=button_count&size=large&mobile_iframe=false&appId=183656348857243&width=164&height=28" width="164" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        var captchaResponce = null;

        function reCaptcha(data) {
            captchaResponce = data;
        }

        function onSubmit(event) {

            if (captchaResponce) {

                return true;
            }
            alert('Пройдіть ReCAPTCHA');

            return false;
        }
    </script>
@endsection