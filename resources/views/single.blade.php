@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <form action="/single/plus" method="POST" onsubmit="if(!onSubmit(this)){return false}">
                <input type="hidden" name="question" value="{{$question->id}}">
                <h2>{{$question->question}}</h2>
                <img src="{{$candidate->img_url}}">
                <a
                        target="_blank"
                        href="{{$candidate->link}}">
                    <h2>
                        <span>{{$candidate->name}}</span>
                        <span class="circle">({{$questionCandidate->rating}})</span>
                    </h2>
                </a>

                {{ csrf_field() }}
                <div class="g-recaptcha"
                     data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"
                     data-callback="reCaptcha">
                </div>
                <input type="hidden" name="id" value="{{$candidate->id}}">
                <button name="submit" type="submit" value="plus" class="btn btn-primary">
                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                </button>
                <button name="submit" type="submit" value="minus" class="btn btn-primary">
                    <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                </button>

            </form>
        </div>
    </div>

    <div class="fb-share-button">
        <iframe src="https://www.facebook.com/plugins/share_button.php?href=https://wobo.herokuapp.com&layout=button_count&size=large&mobile_iframe=false&appId=183656348857243&width=164&height=28" width="164" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        var captchaResponce = null;

        function reCaptcha(data) {
            captchaResponce = data;
        }

        function onSubmit(event) {

            if (captchaResponce) {

                return true;
            }
            alert('Пройдіть ReCAPTCHA');

            return false;
        }
    </script>
@endsection