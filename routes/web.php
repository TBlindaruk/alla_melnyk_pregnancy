<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SingleController@show')->name('single');
Route::post('/single/plus','SingleController@vote');

Route::get('/comparison','ComparisonController@show')->name('comparison');
Route::post('/comparison/vote','ComparisonController@vote');