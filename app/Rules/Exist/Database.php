<?php

namespace App\Rules\Exist;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class CandidateId
 *
 * @package App\Rules
 */
abstract class Database implements Rule
{
    /**
     * @var string
     */
    protected $modelName;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     *
     * @throws \LogicException
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($this->modelName)) {
            throw new \LogicException(sprintf('Please fill the $modelName parameter in %s class', get_class($this)));
        }

        return isset($this->modelName::find($value)->id);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Sorry, we did not have this %s in out DB ', (new \ReflectionClass($this))->getShortName());
    }
}