<?php

namespace App\Rules\Exist;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class CandidateId
 *
 * @package App\Rules
 */
class CandidateId extends Database implements Rule
{
    /**
     * @var string
     */
    protected $modelName = 'App\Candidate';
}