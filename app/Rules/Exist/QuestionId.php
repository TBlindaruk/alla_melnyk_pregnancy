<?php

namespace App\Rules\Exist;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class QuestionId
 *
 * @package App\Rules
 */
class QuestionId extends Database implements Rule
{
    /**
     * @var string
     */
    protected $modelName = 'App\Question';
}