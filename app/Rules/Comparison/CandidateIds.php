<?php

namespace App\Rules\Comparison;

use App\Candidate;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class CandidateId
 *
 * @package App\Rules
 */
class CandidateIds implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $values = explode(',', $value);

        if (count($values) !== 2) {

            return false;
        }

        foreach ($values as $value) {

            if (empty(Candidate::find($value)->id)) {

                return false;
            }
        }

        if ($values[0] === $values[1]) {

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sorry, we did not have a this customer';
    }
}