<?php

namespace App\Http\Requests\Vote;

use App\Http\Requests\ReCaptcha;
use App\Rules\{
    Comparison\CandidateIds,
    Exist\QuestionId
};

/**
 * Class ComparisonRequest
 *
 * @package App\Http\Requests\Vote
 */
class ComparisonRequest extends ReCaptcha
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'submit' => ['required', new CandidateIds],
            'question' => ['required', new QuestionId]
        ]);
    }
}
