<?php

namespace App\Http\Requests\Vote;

use App\Http\Requests\ReCaptcha;
use App\Rules\Exist\{
    CandidateId,
    QuestionId
};

/**
 * Class SingleRequest
 *
 * @package App\Http\Requests
 */
class SingleRequest extends ReCaptcha
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'submit' => 'in:plus,minus',
            'question' => ['required', new QuestionId],
            'id' => ['required', new CandidateId]
        ]);
    }
}
