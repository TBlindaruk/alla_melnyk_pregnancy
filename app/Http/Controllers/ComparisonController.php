<?php

namespace App\Http\Controllers;

use View;
use App\Http\Requests\Vote\ComparisonRequest;

/**
 * Class ComparisonController
 *
 * @package App\Http\Controllers
 */
class ComparisonController extends VoteController
{
    protected $viewName = 'comparison';

    /**
     * @return mixed
     */
    protected function showViewComposer()
    {
        return View::composer('*', 'App\Http\ViewComposers\ComparisonComposer');
    }

    /**
     * @param ComparisonRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function vote(ComparisonRequest $request)
    {
        $ids = explode(',', $request->get('submit'));
        $questionId = $request->get('question');

        $this->ratingRepository->ratingIncrement($ids[0], $questionId);
        $this->ratingRepository->ratingDecrement($ids[1], $questionId);

        return redirect('/comparison');
    }
}