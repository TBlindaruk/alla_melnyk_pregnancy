<?php

namespace App\Http\Controllers;

use View;
use App\Repository\RatingRepository;

/**
 * Class VoteController
 *
 * @package App\Http\Controllers
 */
abstract class VoteController extends Controller
{
    /**
     * @var RatingRepository
     */
    protected $ratingRepository;

    /**
     * @var string
     */
    protected $viewName;

    /**
     * SingleController constructor.
     *
     * @param RatingRepository $ratingRepository
     */
    public function __construct(RatingRepository $ratingRepository)
    {
        $this->ratingRepository = $ratingRepository;
    }

    /**
     * @return mixed
     */
    abstract protected function showViewComposer();

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view($this->viewName, $this->showViewComposer());
    }
}
