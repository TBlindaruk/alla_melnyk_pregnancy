<?php

namespace App\Http\Controllers;

use View;
use App\Http\Requests\Vote\SingleRequest;

/**
 * Class CandidatesController
 *
 * @package App\Http\Controllers
 */
class SingleController extends VoteController
{
    /**
     * @var string
     */
    protected $viewName = 'single';

    /**
     * @return mixed
     */
    protected function showViewComposer()
    {
        return View::composer('*', 'App\Http\ViewComposers\SingleComposer');
    }

    /**
     * @param SingleRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function vote(SingleRequest $request)
    {
        $questionId = $request->get('question');
        $id = $request->get('id');

        switch ($request->get('submit')) {
            case 'minus':
                $this->ratingRepository->ratingDecrement($id, $questionId);
                break;
            default:
                $this->ratingRepository->ratingIncrement($id, $questionId);
        }

        return redirect('/');
    }
}
