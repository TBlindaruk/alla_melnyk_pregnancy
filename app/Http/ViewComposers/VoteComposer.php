<?php

namespace App\Http\ViewComposers;

use App\Repository\View\Random\IRandom;
use Illuminate\Contracts\View\View;

/**
 * Class VoteComposer
 *
 * @package App\Http\ViewComposers
 */
abstract class VoteComposer
{
    /**
     * @var IRandom
     */
    private $randomViewRepository;

    /**
     * VoteComposer constructor.
     *
     * @param IRandom $randomViewRepository
     */
    public function __construct(IRandom $randomViewRepository)
    {
        $this->randomViewRepository = $randomViewRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        foreach ($this->randomViewRepository->generateRandomView() as $name => $value) {
            $view->with($name, $value);
        }
    }
}