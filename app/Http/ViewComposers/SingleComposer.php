<?php

namespace App\Http\ViewComposers;

use App\Repository\View\Random\Single;

/**
 * Class SingleComposer
 *
 * @package App\Http\ViewComposers
 */
class SingleComposer extends VoteComposer
{
    /**
     * SingleComposer constructor.
     *
     * @param Single $randomSingleViewRepository
     */
    public function __construct(Single $randomSingleViewRepository)
    {
        parent::__construct($randomSingleViewRepository);
    }
}