<?php

namespace App\Http\ViewComposers;

use App\Repository\View\Random\Comparison;

/**
 * Class ComparisonComposer
 *
 * @package App\Http\ViewComposers
 */
class ComparisonComposer extends VoteComposer
{
    /**
     * ComparisonComposer constructor.
     *
     * @param Comparison $randomComparisonViewRepository
     */
    public function __construct(Comparison $randomComparisonViewRepository)
    {
        parent::__construct($randomComparisonViewRepository);
    }
}