<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CandidateQuestion
 *
 * @property $rating int
 *
 * @package App
 */
class CandidateQuestion extends Model
{
    protected $table = 'candidate_question';
}
