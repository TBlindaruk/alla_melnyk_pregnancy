<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Candidate
 *
 * @property $rating string
 *
 * @package App
 */
class Candidate extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'link',
        'img_url',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany('App\Question', 'candidate_question');
    }
}
