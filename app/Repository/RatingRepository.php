<?php
/**
 * Created by PhpStorm.
 * User: t.blindaruk
 * Date: 22.11.17
 * Time: 15:29
 */

namespace App\Repository;

use App\{
    Candidate,
    CandidateQuestion
};

/**
 * Class RatingRepository
 *
 * @package App\Repository
 */
class RatingRepository
{
    /**
     * @param int $candidateId
     * @param int $questionId
     */
    public function ratingIncrement(int $candidateId, int $questionId)
    {
        $this->incrementingDecrementingCanvas($candidateId, $questionId, 1);
    }

    /**
     * @param int $candidateId
     * @param int $questionId
     */
    public function ratingDecrement(int $candidateId, int $questionId)
    {
        $this->incrementingDecrementingCanvas($candidateId, $questionId, -1);
    }

    /**
     * @param int $candidateId
     * @param int $questionId
     * @param int $changes
     */
    protected function incrementingDecrementingCanvas(int $candidateId, int $questionId, int $changes)
    {
        /**@var $candidate Candidate */
        /**@var $manyToMany CandidateQuestion */
        $candidate = Candidate::find($candidateId);
        $candidate->questions()->sync([$questionId], false);
        $manyToMany = CandidateQuestion::where('candidate_id', $candidateId)->where('question_id', $questionId)->first();
        $manyToMany->rating += $changes;
        $manyToMany->save();
    }
}