<?php
/**
 * Created by PhpStorm.
 * User: t.blindaruk
 * Date: 22.11.17
 * Time: 16:05
 */

namespace App\Repository\View\Random;

use App\{
    Question,
    Candidate,
    CandidateQuestion
};

/**
 * Class RandomSingleViewRepository
 *
 * @package App\Repository
 */
class Single implements IRandom
{
    /**
     * @return array
     */
    function generateRandomView(): array
    {
        $question = Question::where('is_single', '1')->inRandomOrder()->first();
        $candidate = Candidate::inRandomOrder()->first();

        $questionId = $question->id;
        $candidateId = $candidate->id;
        $candidate->questions()->sync([$questionId], false);
        $questionCandidate = CandidateQuestion::where('candidate_id', $candidateId)->where('question_id', $questionId)->first();

        return compact('question', 'candidate', 'questionCandidate');
    }
}