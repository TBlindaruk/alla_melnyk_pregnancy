<?php
/**
 * Created by PhpStorm.
 * User: t.blindaruk
 * Date: 22.11.17
 * Time: 16:05
 */

namespace App\Repository\View\Random;

use App\{
    Question,
    Candidate,
    CandidateQuestion
};

/**
 * Class RandomSingleViewRepository
 *
 * @package App\Repository
 */
class Comparison implements IRandom
{
    /**
     * @return array
     */
    function generateRandomView(): array
    {
        $candidates = Candidate::inRandomOrder()->take(2)->get();
        $question = Question::where('is_single', '0')->inRandomOrder()->first();

        $firstCandidate = $candidates[0];
        $secondCandidate = $candidates[1];

        $firstCandidateId = $firstCandidate->id;
        $secondCandidateId = $secondCandidate->id;
        $questionId = $question->id;

        $firstCandidate->questions()->sync([$questionId], false);
        $secondCandidate->questions()->sync([$questionId], false);
        $firstQuestionCandidate = CandidateQuestion::where('candidate_id', $firstCandidateId)->where('question_id', $questionId)->first();
        $secondQuestionCandidate = CandidateQuestion::where('candidate_id', $secondCandidateId)->where('question_id', $questionId)->first();

        $questionCandidates = [$firstQuestionCandidate, $secondQuestionCandidate];

        return compact('candidates', 'question', 'questionCandidates');
    }
}