<?php

namespace App\Repository\View\Random;

/**
 * Interface IRandom
 *
 * @package App\Repository\View\Random
 */
interface IRandom
{
    /**
     * @return array
     */
    function generateRandomView(): array;
}