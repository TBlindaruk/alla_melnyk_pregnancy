<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 *
 * @property $question string
 * @property $id int
 * @property $is_single
 *
 * @package App
 */
class Question extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'question',
        'is_single',
    ];
}
