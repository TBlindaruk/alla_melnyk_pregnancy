<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CandidatesTable
 */
class CandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('link')->unique();
            $table->string('img_url');
            $table->timestamps();
        });

        Excel::load(storage_path('imports') . '/friends.csv',function ($reader){
            foreach ($reader->get() as $row) {
                Db::table('candidates')->insert([
                    'name' => $row->name,
                    'link' => $row->link,
                    'img_url' => $row->img_url
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
