<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class QuestionsTable
 */
class QuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->boolean('is_single')->default(true);
            $table->timestamps();
        });

        Excel::load(storage_path('imports') . '/questions.csv', function ($reader) {
            foreach ($reader->get() as $row) {
                Db::table('questions')->insert([
                    'question' => $row->question,
                    'is_single' => $row->is_single,
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
