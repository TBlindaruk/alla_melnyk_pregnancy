### Some useful commands for the project

#### laravel
 - `php artisan migrate` - use for migrate information to db
 - `php artisan migrate --seed` - use for migrate seed to db
 - `php artisan make:migration [migration_file_name]` - use for create a empty migration file with name migration_file_name
 - `php artisan make:seeder [seederName]` - create a seeder file with name [seederName] 
 - `php artisan config:clear` - flush config cache
 - `php artisan cache:clear` - flush cache